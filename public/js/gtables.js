const GTABLES_KEY = "123cJ2GWMG_hPjwsXZH2NDXVhi1Fryo9FzJb5d7TFu2I";

const queryGTables = async page => {
	const url = `https://spreadsheets.google.com/feeds/cells/${GTABLES_KEY}/${page}/public/full?alt=json`;
	const result = await (await fetch(url)).json();

	let rows = [];
	for(const cell of result.feed.entry) {
		const row = cell.gs$cell.row;
		rows[row] = rows[row] || [];
		rows[row][cell.gs$cell.col - 1] = cell.gs$cell.$t;
	}
	return rows.filter(r => r);
};