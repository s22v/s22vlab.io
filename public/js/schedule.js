const querySchedule = async () => {
	const table = await queryGTables(2);
	return table[0].map((dayOfWeek, j) => {
		const classes = [];
		let i = 1;
		while(table[i] && table[i][j]) {
			classes.push(table[i++][j]);
		}
		return {classes, dayOfWeek, dayOfWeekI: j};
	});
};

(async () => {
	let currentDayOfWeekI = (new Date()).getDay();
	if(currentDayOfWeekI === 0) {
		currentDayOfWeekI = 7;
	}
	currentDayOfWeekI--;

	for(const {classes, dayOfWeek, dayOfWeekI} of await querySchedule()) {
		const day = document.createElement("div");

		const cell = document.createElement("div");
		cell.textContent = dayOfWeek;
		cell.className = "cell cell-heading";
		if(dayOfWeekI === (currentDayOfWeekI + 1) % 7) {
			day.className = "tomorrow";
			cell.textContent += " (завтра)";
		}
		day.appendChild(cell);

		for(const {name, i} of classes.map((name, i) => ({name, i}))) {
			const cell = document.createElement("div");
			cell.textContent = `${i + 1}. ${name}`;
			cell.className = "cell";
			day.appendChild(cell);
		}

		document.querySelector(".schedule").appendChild(day);
	}
})();