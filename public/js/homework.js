const queryHomework = async () => {
	return (await queryGTables(1)).slice(1).map(([subject, deadline, homework]) => ({subject, deadline, homework}));
};

(async () => {
	for(const row of await queryHomework()) {
		const tr = document.createElement("tr");

		const appendCell = (text, className) => {
			const td = document.createElement("td");
			td.textContent = text.replace(/\\n/g, "\n");
			td.className = className;
			tr.appendChild(td);
		};

		appendCell(row.subject, "subject");
		appendCell(row.deadline, "deadline");
		appendCell(row.homework, "homework");

		document.querySelector(".homework").appendChild(tr);
	}
})();